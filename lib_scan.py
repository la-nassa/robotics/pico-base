from machine import Pin, time_pulse_us
import time
import utime
from variables import *

# SENSORE DI DISTANZA
prossi_trig = Pin(U_SENSOR_PIN_TRIGGER, Pin.OUT)  # verde
prossi_echo = Pin(U_SENSOR_PIN_ECHO, Pin.IN, Pin.PULL_DOWN)  # viola
timeout_pulse = 20000 # us
delay_start = 10  # us
delay_end = 10000 # us
sound_speed = 0.000343 # m/us
max_distance = timeout_pulse*sound_speed

def distanza():

    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);
    prossi_trig.value(1)  # digitalWrite(trigPin, HIGH);
    utime.sleep_us(delay_start)  # delayMicroseconds(10);
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);

    duration = time_pulse_us(prossi_echo, 1, timeout_pulse)
    if duration == -1:
        print("la distanza è troppo grande") 
        return max_distance 
    if duration == -2:
        print("il sensore non sembra essere stato triggerato correttamente")
        return max_distance
    print("Durata:", duration)
    distance = duration * sound_speed / 2
    #print("Distance: ", distance)
    utime.sleep_us(delay_end) 
    return distance


def distanza_hard_coded():
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);
    prossi_trig.value(1)  # digitalWrite(trigPin, HIGH);
    utime.sleep_us(10)  # delayMicroseconds(10);
    prossi_trig.value(0)  # digitalWrite(trigPin, LOW);

    wait_count = 0
    while prossi_echo.value() == 0:
        wait_count = wait_count+1
        if wait_count > 5000:
            print("skip to next measurement")
            return 1000
    StartTime = utime.ticks_us()
    # save time of arrival
    while prossi_echo.value() == 1:
        continue
    StopTime = utime.ticks_us()

    duration = StopTime - StartTime
    #print(duration)
    distance = duration * sound_speed / 2
    #print("Distance: ", distance)
    #print(distance)
    #print(" cm")
    time.sleep(0.1)
    return distance
