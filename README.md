# pico-base
La versione base del codice per muovere il robottino con due motori e un sensore a ultrasuoni per calcolare la distanza dagli ostacoli.

Il robot si ferma quando vede un ostacolo e riparte quando l'ostacolo viene rimosso.
Controlla ogni ciclo (la durata del ciclo dipende dalle variabili `TIME_TO_WAIT` e `TIME_TO_CHECK`)
Ogni `ENOUGH_MOVEMENTS` esecuzioni del ciclo gira a destra (o sinistra, alternato) e poi riparte.
