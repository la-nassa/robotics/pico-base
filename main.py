import time
import lib_mov
from variables import *
import lib_scan


# stop e aspetta un secondo
lib_mov.stop()
time.sleep(1)

print("aspettando il segnale di partenza..")
triggered = False
while not triggered:
    distance = lib_scan.distanza()
    if distance < THRES_START:
        triggered = True

print('si parte!')
lib_mov.avanti_tutta()
# inizialmente va avanti un po' per evitare di fermarsi subito
time.sleep(0.2)
counter = 0
turn = right

# loop infinito
while True:
    time.sleep(TIME_TO_WAIT)
    distance = lib_scan.distanza()

    # se troviamo un ostacolo, ci fermiamo
    if distance < THRES_OBSTACLE:
        print("Ostacolo?")
        time.sleep(TIME_TO_CHECK)
        distance = lib_scan.distanza()
        if distance < THRES_OBSTACLE:
            print("Ostacolo trovato!")
            lib_mov.stop()
    if distance > THRES_OBSTACLE:
        print("liberi?")
        time.sleep(TIME_TO_CHECK)
        distance = lib_scan.distanza()
        if distance > THRES_OBSTACLE:
            print("liberi! si riparte!")
            lib_mov.avanti_tutta()

    counter += 1
    if counter > ENOUGH_MOVEMENTS:
        counter = 0
        if turn == 'right':
            print("giriamo a destra")
            lib_mov.gira_avanti_dx(2)
            turn = 'left'
        elif turn == 'left':
            print("giriamo a sinistra")
            lib_mov.gira_avanti_sx(2)
            turn = 'right'

print("FINISHED")
