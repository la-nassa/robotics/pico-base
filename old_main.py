from machine import Pin, PWM
import time
import utime
import lib_mov
from variables import *
#stop, gira_avanti_dx,gira_avanti_sx,avanti_tutta,
#import random
#time.sleep(100000)
servo = PWM(Pin(18))  # arancio
servo.freq(50)
servo.duty_ns(dritto)
step_pwm = 20000  # old 80000
n_step_fullscan = int((estrema_sn-estrema_dx)/step_pwm)
verso = 1

# wait 3 seconds before moving and blink once
lib_mov.stop()
time.sleep(1)


print('starting while loop')
#lib_mov.avanti_tutta()
counter = 1
while True:
    distance, verso = lib_mov.scan(
        servo, verso, estrema_dx, estrema_sn, step_pwm)
    if distance < soglia_ostacolo:
        distance = lib_mov.distanza()
        if distance < soglia_ostacolo:
            distance = lib_mov.distanza()
            if distance < soglia_alt:
                lib_mov.stop()
                servo_time = 0.0001  # serve davvero? non sono sicuro
                servo.duty_ns(estrema_dx+1000)
                time.sleep(servo_time)
                list_distance = []
                for i in range(n_step_fullscan):
                    verso = 1
                    distance, verso = lib_mov.scan(
                        servo, verso, estrema_dx, estrema_sn, step_pwm)
                    time.sleep(servo_time)
                    list_distance.append(distance)
                min_pos = list_distance.index(min(list_distance))
                if min_pos >= n_step_fullscan/2:
                    print("ostacolo sembra piu vicino a sx, giro a dx all'indietro")
                    lib_mov.indietro_tutta(0.1)
                    lib_mov.gira_indietro_dx(0.3)
                    time.sleep(0.5)
                else:
                    print("ostacolo sembra piu vicino a dx, giro a sx all'indietro")
                    lib_mov.indietro_tutta(0.1)
                    lib_mov.gira_indietro_sx(0.3)
                    time.sleep(0.5)
                lib_mov.avanti_tutta()
            elif distance < soglia_ostacolo:  # conferma che la distanza rilevata e' corretta
                print("ostacolo")
                #lib_mov.stop() #da eliminare se funziona bene
                if servo.duty_ns() < dritto:
                    print("ostacolo a dx, giro a sn")
                    lib_mov.gira_avanti_sx(0.2)
                if servo.duty_ns() >= dritto:
                    print("ostacolo a sx, giro a dx")
                    lib_mov.gira_avanti_dx(0.2)
                time.sleep(0.5)
                lib_mov.avanti_tutta()

counter = counter+1
time.sleep(servo_time)
